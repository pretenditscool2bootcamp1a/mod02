"use strict";
function parseConfCode(inpCode) {
    let inpArray;
    let prodType = "";
    let prodSize = "";
    let prodSizeConv = "";
    inpArray = inpCode.split("-");
    prodType = inpArray[0];
    prodSize = inpArray[1];
    prodSizeConv = prodSize == "L" ? "Large" : prodSize == "M" ? "Medium" : prodSize == "S" ? "Small" : prodSize = "XL" ? "Extra Large" : "Unknown Size";
    var pickupDate = inpArray[2].substring(0, 2) + "-" + inpArray[2].substring(2);
    console.log(prodSizeConv + " " + capitalizeFirstLetter(prodType) + " picked up on " + pickupDate);
}
function capitalizeFirstLetter(a) {
    a = a.toLowerCase();
    return a.charAt(0).toUpperCase() + a.slice(1);
}
parseConfCode("HAM-L-1220");
parseConfCode("TURKEY-M-1125");
parseConfCode("PICKLES-S-0122");
parseConfCode("GIN-XL-0224");
