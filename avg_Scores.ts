let scores: number[] = [93, 86, 73, 79, 83, 100, 94] ;
let sum: number = 0 ;
for (let i: number = 0 ; i < scores.length; i++) {
    sum = sum + scores[i] ;
}

let average : number = sum / scores.length ; 
let sortedScores = scores.sort (function(a: number, b: number){return a-b} ) ;



function displayStat ( statType: string, inpNumber: number ): void {
   
    let xtoChar: string = String(inpNumber) ;
    console.log (statType + " " + xtoChar) ;

}

displayStat("average" , average) ;
displayStat("High Score", sortedScores[0]) ;
displayStat("Low Score", sortedScores[sortedScores.length - 1]) ;
