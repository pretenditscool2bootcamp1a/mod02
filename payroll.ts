let payRate: number;
let hoursWorked: number;
let filingStatus: boolean;

function weeklyGrossPay(x: number, y: number): number {
    let grosspayatbaserate: number = 0;
    let overtimegrosspay: number = 0;
    if (y <= 40) {
        grosspayatbaserate = x * y;
    }
    else {
        overtimegrosspay = (x * 1.5) * (y - 40);
    }
    return overtimegrosspay + grosspayatbaserate;
}

function annualGrossPay(x: number): number {
    return x * 52;
}

function findTaxRate(x: number, y: boolean): number {
    let calculatedTaxRate: number = 0;
    if (y === false) {
        if (x < 23000) {
            calculatedTaxRate = .05;
        }
        else if (x >= 23000 && x <= 74999.99) {
            calculatedTaxRate = .12;
        }
        else {
            calculatedTaxRate = .2;
        }
    }
    else {
        if (x < 23000) {
            calculatedTaxRate = 0;
        }
        else if (x >= 23000 && x <= 74999.99) {
            calculatedTaxRate = .09;
        }
        else {
            calculatedTaxRate = .2;
        }
        
    }
    return calculatedTaxRate;
}

function payInfo ( payRate: number, weeklyHours: number, jointFiling: boolean): void {
    console.log ("You worked " + weeklyHours + " hours this period.") ;
    console.log ("because you earn " + payRate + " per hour, your gross pay is $" + weeklyGrossPay(payRate, weeklyHours)) ;
    console.log("Your filing status is " + jointFiling) ;
    console.log("Your tax witholding this period is $" + weeklyGrossPay(payRate, weeklyHours) * findTaxRate( annualGrossPay(weeklyGrossPay(payRate, weeklyHours)) , jointFiling));
    console.log("Your net pay is $" + 
    (weeklyGrossPay(payRate, weeklyHours) - weeklyGrossPay(payRate, weeklyHours) * findTaxRate( annualGrossPay(weeklyGrossPay(payRate, weeklyHours)) , jointFiling) )
    ) ;
}