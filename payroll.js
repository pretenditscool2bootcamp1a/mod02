"use strict";
let payRate;
let hoursWorked;
let filingStatus;
function weeklyGrossPay(x, y) {
    let grosspayatbaserate = 0;
    let overtimegrosspay = 0;
    if (y <= 40) {
        grosspayatbaserate = x * y;
    }
    else {
        overtimegrosspay = (x * 1.5) * (y - 40);
    }
    return overtimegrosspay + grosspayatbaserate;
}
function annualGrossPay(x) {
    return x * 52;
}
function findTaxRate(x, y) {
    let calculatedTaxRate = 0;
    if (y === false) {
        if (x < 23000) {
            calculatedTaxRate = .05;
        }
        else if (x >= 23000 && x <= 74999.99) {
            calculatedTaxRate = .12;
        }
        else {
            calculatedTaxRate = .2;
        }
    }
    else {
        if (x < 23000) {
            calculatedTaxRate = 0;
        }
        else if (x >= 23000 && x <= 74999.99) {
            calculatedTaxRate = .09;
        }
        else {
            calculatedTaxRate = .2;
        }
    }
    return calculatedTaxRate;
}
function payInfo(payRate, weeklyHours, jointFiling) {
    console.log("You worked " + weeklyHours + " hours this period.");
    console.log("because you earn " + payRate + " per hour, your gross pay is $" + weeklyGrossPay(payRate, weeklyHours));
    console.log("Your filing status is " + jointFiling);
    console.log("Your tax witholding this period is $" + weeklyGrossPay(payRate, weeklyHours) * findTaxRate(annualGrossPay(weeklyGrossPay(payRate, weeklyHours)), jointFiling));
    console.log("Your net pay is $" +
        (weeklyGrossPay(payRate, weeklyHours) - weeklyGrossPay(payRate, weeklyHours) * findTaxRate(annualGrossPay(weeklyGrossPay(payRate, weeklyHours)), jointFiling)));
}
