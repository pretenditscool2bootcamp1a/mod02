"use strict";
let scores = [93, 86, 73, 79, 83, 100, 94];
let sum = 0;
for (let i = 0; i < scores.length; i++) {
    sum = sum + scores[i];
}
let average = sum / scores.length;
let sortedScores = scores.sort(function (a, b) { return a - b; });
function displayStat(statType, inpNumber) {
    let xtoChar = String(inpNumber);
    console.log(statType + " " + xtoChar);
}
displayStat("average", average);
displayStat("High Score", sortedScores[0]);
displayStat("Low Score", sortedScores[sortedScores.length - 1]);
