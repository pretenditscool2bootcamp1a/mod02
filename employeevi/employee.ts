class employee {
    private id: number;
    public name: string;
    public jobTitle: string;
    public payRate: number;

    constructor(id: number, name: string, jobTitle: string, payRate: number) {
        this.id = id;
        this.name = name;
        this.jobTitle = jobTitle;
        this.payRate = payRate ;
        }

    toString() : string {
        return `${this.name} (${this.id}) is a ${this.jobTitle} earning \$${this.payRate}/hr` ;
    }
}

let emp1 = new employee(1, "stephen", "brick layer", 45.50) ;
console.log (emp1.toString()) ;

let emp2 = new employee(5, "rhonda", "architect", 225.25) ;
console.log (emp2.toString()) ;