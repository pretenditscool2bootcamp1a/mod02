var employee = /** @class */ (function () {
    function employee(id, name, jobTitle, payRate) {
        this.id = id;
        this.name = name;
        this.jobTitle = jobTitle;
        this.payRate = payRate;
    }
    employee.prototype.toString = function () {
        return "".concat(this.name, " (").concat(this.id, ") is a ").concat(this.jobTitle, " earning $").concat(this.payRate, "/hr");
    };
    return employee;
}());
var emp1 = new employee(1, "stephen", "brick layer", 45.50);
console.log(emp1.toString());
var emp2 = new employee(5, "rhonda", "architect", 225.25);
console.log(emp2.toString());
